﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	[SerializeField]
	private Text chuteText;

	public GameObject difficultyPanel;
	public GameObject endGamePanel;

	void Awake(){
		difficultyPanel.SetActive(true);
		endGamePanel.SetActive(false);
	}

	public void mostrarNovoChute(int novoChute){
		chuteText.text = "O Número é " + novoChute + "?";
	}

	public void DisableDifficultyPanel(){
		difficultyPanel.GetComponent<Animator>().Play("DiffMenuClosing");
	}

	public void EnableEndGamePanel(){
		endGamePanel.SetActive(true);
		endGamePanel.GetComponent<Animator>().Play("EndGameOpening");
	}
}
