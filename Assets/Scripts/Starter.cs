﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Starter : MonoBehaviour {
	public bool isSmart;
	GameControllerHuman gameController;
	[SerializeField]
	private UIManager uiManager;
	private void IniciarJogo(){
		int menor = 0, maior = 100;
		if(isSmart){
			gameController = new GameControllerSmart(menor, maior);
		}else{
			gameController = new GameControllerHuman(menor, maior);
		}
		gameController.PrepararJogo();
		uiManager.mostrarNovoChute(gameController.getChuteAtual());
	}

	public void Maior(){
		gameController.SeriaMaior();
		uiManager.mostrarNovoChute(gameController.getChuteAtual());
	}

	public void Menor(){
		gameController.SeriaMenor();
		uiManager.mostrarNovoChute(gameController.getChuteAtual());
	}

	public void Acertou(){
		uiManager.EnableEndGamePanel();
	}

	public void SetHuman(){
		this.isSmart = false;
		uiManager.DisableDifficultyPanel();
		IniciarJogo();
	}

	public void SetSmart(){
		this.isSmart = true;
		uiManager.DisableDifficultyPanel();
		IniciarJogo();
	}

	public void ReiniciarJogo(){
		SceneManager.LoadScene("Game");
	}

	public void VoltarMenu(){
		SceneManager.LoadScene("MainMenu");
	}
}
