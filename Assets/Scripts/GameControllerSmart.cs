﻿using UnityEngine;

public class GameControllerSmart : GameControllerHuman {

	public GameControllerSmart(int menor, int maior) : base(menor, maior){
		this.menorValorPossivel = menor;
		this.maiorValorPossivel = maior;
	}
	
	public override void realizarJogada(){
		chuteAtual = (menorValorPossivel + maiorValorPossivel)/2;
	}
}
