﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControllerHuman{
	public int chuteAtual;
	public int menorValorPossivel;
	public int maiorValorPossivel;

	public GameControllerHuman(int menorValorPossivel, int maiorValorPossivel){
		this.menorValorPossivel = menorValorPossivel;
		this.maiorValorPossivel = maiorValorPossivel;
	}

	public void PrepararJogo(){
		realizarJogada();
	}

	public virtual void realizarJogada(){
		chuteAtual = Random.Range(menorValorPossivel, maiorValorPossivel);
	}

	public void SeriaMenor(){
		maiorValorPossivel = chuteAtual--;
		realizarJogada();
	}

	public void SeriaMaior(){
		menorValorPossivel = chuteAtual++;
		realizarJogada();
	}

	public int getChuteAtual(){
		return chuteAtual;
	}
}
