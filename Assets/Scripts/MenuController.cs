﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public void IniciarJogo(){
		SceneManager.LoadScene("Game");
	}

	public void SairJogo(){
		Application.Quit();
	}
}
